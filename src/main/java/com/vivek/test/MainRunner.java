package com.vivek.test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class MainRunner {

    public static void main(String[] args) throws MalformedURLException{
        System.out.println("Reading https://www.apple.com/mac/");
        URL url = new URL("https://www.apple.com/mac/");
        URLKeyNameFinder finder = new URLKeyNameFinder(url, "compare-headline typography-compare-headline");
        List<String> x = finder.getString();
        x.stream().forEach(b -> System.out.println(b));

        System.out.println("Reading https://www.apple.com/ipad/");

        URL url2 = new URL("https://www.apple.com/ipad/");
        URLKeyNameFinder finder2 = new URLKeyNameFinder(url2, "typography-label");
        List<String> x2 = finder2.getString();
        x2.stream().forEach(b -> System.out.println(b));
    }
}
