package com.vivek.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class URLreader {
    private static final int CONNECTION_TIMEOUT_MILLS = 15000;
    private static final int READ_TIMEOUT_MILLS = 15000;
    private URL url;

    public URLreader(URL url){
        this.url = url;
    }

    public Optional<InputStream> urlToInputStream(URL url) {
        HttpURLConnection con = null;
        InputStream inputStream = null;
        try {
            con = (HttpURLConnection) url.openConnection();
            con.setConnectTimeout(CONNECTION_TIMEOUT_MILLS);
            con.setReadTimeout(READ_TIMEOUT_MILLS);
            con.connect();
            int responseCode = con.getResponseCode();
            if (responseCode == 200) {
                inputStream = con.getInputStream();
                return Optional.of(inputStream);
            }
        } catch (IOException e) {
            throw new IllegalStateException("Unable to read value from URL");
        }
        return Optional.empty();
    }

    public List<String> getLines() {
        List<String> result = new ArrayList<>();
        InputStream stream = urlToInputStream(url).orElseThrow(
                () -> new IllegalStateException("Unable to fetch details from URL")
        );
        BufferedReader br = new BufferedReader(new InputStreamReader(stream));
        try {
            String temp = br.readLine();
            while (temp != null) {
                result.add(temp);
                temp = br.readLine();
            }
        } catch (Exception ex) {
            System.err.println("Error in reading URL response" + ex);
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException ex) {
                    System.err.println("Error in closing stream" + ex);
                }
            }
        }
        return result;
    }

}
