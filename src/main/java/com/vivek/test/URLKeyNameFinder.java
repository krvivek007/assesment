package com.vivek.test;

import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


public class URLKeyNameFinder {

    private URL url;
    private String key;

    public URLKeyNameFinder(URL url, String key){
        this.url = url;
        this.key = key;
    }

    public List<String> getString() {
        URLreader urLreader = new URLreader(this.url);
        return urLreader.getLines()
                .stream()
                .filter(x -> x.contains(key))
                .map(x -> extractMatchingName(x))
                .filter(Optional::isPresent)
                .map(Optional::get )
                .map(x -> decodeHtml(x))
                .collect(Collectors.toList());
    }

    public String decodeHtml(String inputString){
        return inputString.replaceAll("&nbsp;", " ");
    }

    public Optional<String> extractMatchingName(String lineItem){
        try {
            return Optional.of(URLDecoder.decode(lineItem.split(">")[1].split("<")[0],
                    StandardCharsets.UTF_8.toString()));
        }catch (Exception ex){
            System.err.println("Error in extracting macting name with key: " + this.key );
        }
        return Optional.empty();
    }
}
