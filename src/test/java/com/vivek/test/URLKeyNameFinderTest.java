package com.vivek.test;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;

/**
 * Unit test for simple App.
 */
public class URLKeyNameFinderTest
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public URLKeyNameFinderTest(String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( URLKeyNameFinderTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testExtractMatchingName() throws MalformedURLException {
        URL url = new URL("https://www.apple.com/mac/");
        URLKeyNameFinder finder = new URLKeyNameFinder(url, "compare-headline typography-compare-headline");
        Optional x = finder.extractMatchingName("<h3 class=\"compare-headline typography-compare-headline\">MacBook&nbsp;Pro&nbsp;" +
                "13”</h3>");
        assertTrue( x.isPresent() );
        assertEquals("MacBook&nbsp;Pro&nbsp;13”", x.get());
    }
}
